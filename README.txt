# Druparcheky Basics (same as Profile)

Drupal 8/9 profile to quick start develop

## Download

``
composer require carcheky/druparcheky_basics
``

or

* this option need to manually Download modules
``
composer require drupal/druparcheky_basics
``

## require

- admin_toolbar
- ctools
- devel
- layout_section_classes
- module_filter
- pathauto
- token
